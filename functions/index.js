const functions = require('firebase-functions');
const vision = require('@google-cloud/vision');

const client = new vision.ImageAnnotatorClient();

const DATE_INDEX = 0;
const ACTTIME_INDEX = 5;
const CAL_INDEX = 6;
const DIST_INDEX = 7;

const Client = require('pixela-node').default;
const pixelaClient = new Client();
pixelaClient.username = functions.config().pixela.username;
pixelaClient.token = functions.config().pixela.token;

exports.ringfit = functions
  .region('asia-northeast1')
  .https.onRequest(async (request, response) => {
    const actInfo = await detectActivityInfo(request.body.URL);
    await recodePixel(actInfo);
    
    response.send(actInfo);
});

async function detectActivityInfo(url) {
  console.log(url);
  
  // Vision APIで画像からテキスト読み取り
  const [result] = await client.textDetection(url);
  const detections = result.textAnnotations;

  // 活動日付、活動時間、消費カロリー、距離を検出
  console.log(detections);
  let text = detections[0].description;
  text = text.split('\n');
  console.log(text);
  
  const actdate = text[DATE_INDEX];
  const acttime = text[ACTTIME_INDEX];
  const cal = text[CAL_INDEX];
  const dist = text[DIST_INDEX];
  
  return {'date':actdate, 'time':acttime, 'cal':cal, 'dist':dist};
  
}

function formatDate(date) {
  return new Date().getFullYear() + date.split('(')[0].replace('/', '')
}

function timecode2min(time) {
  splitTime = time.split(':');
  min = parseInt(splitTime[0])*60 + parseInt(splitTime[1]) + parseInt(splitTime[2])/60;
  console.log(min);
  
  return min.toFixed(2);
}

async function recodePixel(actInfo) {
  const date = formatDate(actInfo.date);
  const time = timecode2min(actInfo.time);
  const cal = actInfo.cal.replace('kcal', '');
  const distance = actInfo.dist.replace('km', '');
  
  await Promise.all([
    pixelaClient.UpdatePixel(functions.config().pixela.graphs.time, date, time),
    pixelaClient.UpdatePixel(functions.config().pixela.graphs.time, date, cal),
    pixelaClient.UpdatePixel(functions.config().pixela.graphs.time, date, distance)
  ]);
}